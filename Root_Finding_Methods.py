# -*- coding: utf-8 -*-
"""
Created on Sun July 12 07:35:44 2020

@author: 17703
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy import misc as ms

x=np.linspace(0,.5*np.pi,1000)
y=.5*np.sin(x)-x+.5
yprime=.5*np.cos(x)-1
plt.xlabel("x")
plt.ylabel("y")
plt.title(".5*sinx-x+.5")
plt.plot(x,y)
tol=10**(-7)

########################################### Number 1 #########################################################
print("Number 1:")

def f(x):
    return .5*np.sin(x)-x+.5


################### Bisection Method ###########################
print("(a) Bisection Method")
a=0
b=.5*np.pi
j=0
while np.abs(b-a)>=tol:
    if f(a)*f(b)>=0:
        print("Bisection method is invalid")
    c=(a+b)/2
    if f(b)*f(c)>0:
        b=c
    if f(b)*f(c)<0:
        a=c
    if f(b)*f(c)==0: 
        print("root=",c)
        break
    if f(a)==0:
        print("root=",a)
    if f(b)==0:
        print("root=",b)
    j=j+1
    print("k=",j,",x_k=",c)
print("Bisection Method root=",(a+b)/2)
print("Number of Iterations for Bisection Method=",j)
print("")
################# Newtons Method ################################
print("(c) Newtons Method")
x0=3
x1=0
i=0

def fprime(f,x0):
    return ms.derivative(f,x0)
while np.abs(x1-x0)>=tol:
    x0=x1
    x1=x0-(f(x0)/fprime(f,x0))
    i=i+1
    print("k=",i,",x_k=",x1)
print("")
print("Newton Method Root=",x0)
print("Number of Iterations for Newtons Method=",i)
print("")
####################### Secant Method ##########################
print("(d) Secant Method")
x2=0
x3=0
x4=1
k=0
while np.abs(x2-x4)>=tol:
    x2=x3
    x3=x4
    x4=x2-((f(x2)*(x2-x3))/(f(x2)-f(x3)))
    k=k+1
    print("k=",k,",x_k=",x4)
print("")
print("Secant Method=",x3)
print("Number of Iterations for Secant Method",k)
print("")
################### Fixed Point Algorithm ###########################
print("(b) Fixed Point Algorithm")
def g1(x):
    return .5*np.sin(x)+.5

xk1=1
xk2=0
m=0
while np.abs(xk1-xk2)>=tol:
    xk1=xk2
    xk2=g1(xk1)
    m=m+1
    print("k=",m,",x_k=",xk2)
def g1prime(x):
    return ms.derivative(g1,x)
rho=np.abs(g1prime(xk2))
print("")
print("Fixed Point Algorithm root=",xk1)
print("Number of Iterations for Fixed Point Algorithm=",m)   
##################################################### Number 2 ##########################################################
print("")
print("Number 2:")
print("(a) The fastest method that converged was the Secant Method and it took 6 iterations to converge.")
print("Bisection Method Convergence Rate=",1/j)
print("Newton Method Convergence Rate=",1/i)
print("Secant Method Convergence Rate=",1/k)
print("Fixed Point Algorithm Convergence Rate=",1/m)
#print("Fixed Point Method convergence rate=-np.log10(np.abs(rho))=",-np.log10(np.abs(np.cos(xk1)/2)))
print("")
print("(b) f(0)=sin(0)/2-0+(1/2)=(1/2) and f(pi/2)=sin(pi/2)/2-(pi/2)+(1/2)=1-pi/2<0. Since f(0)>0 and f(pi/2)<0, the Intermediate Value Theorem states there must be a zero on C[0,(pi/2)]. Therefore the bisection method is valid with the initial interval [0,(pi/2)]") 
print("rho<1. The Fixed Point Theorem tells wheter a root is unique in an arbitrary interval. To do this we must show that x values within this interval will produce an abs(g'(x))<=rho.")
print("abs(g'(x))=abs(cosx/2)<1. Since 0<=abs(cosx)<=1, 0<=abs(cosx/2)<=1/2<1. Therefore there exist a unique root between [0,pi/2].")
##################################################### Number 3 ##########################################################
print("")
print("Number 3:")
plt.figure()
x=np.linspace(-10,10,1000)
plt.plot(x,np.exp(x)-x-1)

def g(x):
    return np.exp(x)-x-1
print("The bisection method is not suitable because the range of the function is x>=0. The bisection method needs to have at least one negative f(x) bound to begin its iteration.")
print("The Newton and Secant methods will not always converge where the root is at local minimum/maximum.")
################### Fixed Point Algorithm ###########################
def g2(x):
    return np.exp(x)-1
xk1=1
xk2=0
m=0
while np.abs(xk1-xk2)>=tol:
    xk1=xk2
    xk2=g2(xk1)
    m=m+1
print("")
print("Langston's Fixed Point Algorithm root=",xk1)
print("The Fixed Point Algoritm is the only iterative method that will converge to the correct root.")
















