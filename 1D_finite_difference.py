# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 13:35:15 2020

@author: Langston Williams

Description: This code provides a finite differencing framework to solve up to a second-order ODE. The first example solves a first order differential equation
(DEq) and the second example solves a second order DEq. These examples use difference matrices that essentially takes a derivative to the dependent variable. You
can change this codes boundary conditions and rearrange its difference matrices to solve a variety of second-order ODEs. 
"""

import matplotlib.pyplot as plt
import numpy as np

plot_title1="Solve du/dx+u=3 where u(0)=0"
n=60                                                                           # number of points
a=0                                                                             # left boundary x value
b=1                                                                             # right boundary x value

h=(b-a)/(n-1)                                                                   # step size

A=np.zeros((n,n))                                                               # first-order derivative matrix                                    
I=np.zeros((n,n))                                                               # identity matrix
for i in range(n):
    I[i][i]=1
for i in range(n):
    A[i][i]=1
    if i>0:
        A[i][i-1]=-1
    else: A[i][i-1]=0

Inv_Matrix=np.linalg.inv(A/h+I)                                                 # inverse matrix to A to solve Ax=b    
bvec=np.zeros(n)                                                                # non-homogeneous vector 
for i in range(n):
    bvec[i]=3
bvec[0]=0                                                                       # impose boundary condition u(0)=0 at boundary
x=Inv_Matrix.dot(bvec)                                                          # solve for the solution vector x 

Exact=np.zeros(n)                                                               # Exact solution vector
hvec=np.zeros(n)                                                                # domain for finite difference and exact solution        
plt.figure() 
for i in range(n):
    hvec[i]=a+i*h
    Exact[i]=3-3*np.exp(-hvec[i])
plt.plot(hvec,Exact,'o',label='Exact')
plt.plot(hvec,x,'red',label='Backwards Differencing')
plt.legend()
plt.title(plot_title1)

plot_title2="Solve d^2u/dx^2=20*cos(4*pi*x) where u(0)=0, u(1)=-1"

BC_vec=np.zeros((n))
BC_vec[0]=a
BC_vec[-1]=b
h=(b-a)/(n-1)
bvec=np.zeros((n))
for i in range(n):
    bvec[i]=20*np.cos(4*np.pi*(a+h*i))
A_c=np.zeros((n,n))                                                             # second-order derivative matrix
for i in range(n):
    A_c[i][i]=-2
for i in range(n-1):
    A_c[i][i+1]=1
for i in range(n-1):
    A_c[i+1][i]=1
Inv_Matrix_A_c=np.linalg.inv(A_c)
u_c=np.dot(Inv_Matrix_A_c,((h**2)*bvec+BC_vec))
Exact=np.zeros(n)
hvec=np.zeros(n)
plt.figure() 
for i in range(n):
    hvec[i]=a+i*h
    Exact[i]=(5-4*(np.pi**2)*hvec[i]-5*np.cos(4*np.pi*hvec[i]))/(4*np.pi**2)
plt.plot(hvec,Exact,'--',label='Exact')
plt.plot(hvec,u_c,'o',label='Adjusted Backwards Differencing')
plt.legend()
plt.title(plot_title2)
print(A_c)
